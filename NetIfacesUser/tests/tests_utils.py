#!/usr/bin/env python
# coding=utf-8


import os
import re
import signal
import subprocess
import time


def run_command(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (stdout, stderr) = process.communicate()
    return stdout[:-1]


def get_ifconfig_output():
    return run_command('ifconfig -a')


def interrupt_program(pid):
    os.kill(pid, signal.SIGINT)


def parse_ifconfig_output():
    ifaces = {}
    for line in get_ifconfig_output().split('\n'):
        iface_info_found = re.search(r'(\w+)\s*Link encap:(.+)', line)
        if not iface_info_found:
            continue
        name = iface_info_found.group(1)
        iface_info = iface_info_found.group(2)
        ifaces[name] = {}
        l2_address_found = re.search(r'HWaddr ([\w:]+)', iface_info)
        if l2_address_found:
            ifaces[name]['l2_address'] = l2_address_found.group(1)
            ifaces[name]['type'] = re.search(r'(\w*)\s*HWaddr', iface_info).group(1)
        else:
            ifaces[name]['type'] = iface_info.rstrip()
    return ifaces


def parse_user_output(output_file):
    ifaces = []
    events = []
    with open(output_file, 'r') as f:
        for line in f.readlines():
            iface_found = re.search(r'IFACE (\w+)\s+([\w:]+)\s+(.+)', line)
            if iface_found:
                ifaces.append({
                    'name' : iface_found.group(1),
                    'l2_address': iface_found.group(2),
                    'type': iface_found.group(3)
                })

            event_found = re.search(r'(NEW|GONE)\s*(\w+)\s*\w+', line)
            if event_found:
                events.append({
                    'event_type' : event_found.group(1),
                    'iface_name' : event_found.group(2)
                })

    return (ifaces, events)
