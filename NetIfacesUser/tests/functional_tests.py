#!/usr/bin/env python
# coding=utf-8


import unittest
from tests_utils import *

EXECUTABLE_PATH = '../bin/./NetIfacesUser'
LOGFILE_PATH = '/tmp/NetIfaceUser.log'


def run_user(output_file):
    with open(output_file, 'w') as log_file:
        process = subprocess.Popen([EXECUTABLE_PATH], stdout=log_file, shell=False)
        return process.pid


class TestNetIfacesUserListing(unittest.TestCase):
    def setUp(self):
        self.user_pid = run_user(LOGFILE_PATH)

    def preprocess(self):
        interrupt_program(self.user_pid)
        self.user_ifaces = parse_user_output(LOGFILE_PATH)[0]
        self.ifconfig_ifaces = parse_ifconfig_output()

    def tearDown(self):
        os.remove(LOGFILE_PATH)

    def test_user_interfaces_number_in_4s_matches_ifconfig_interfaces_number(self):
        time.sleep(4) # 4s
        self.preprocess()
        self.assertEqual(len(self.user_ifaces), len(self.ifconfig_ifaces))

    def test_user_interfaces_attributes_match_ifconfig_attributes(self):
        time.sleep(4) # 4s
        self.preprocess()
        for ui in self.user_ifaces:
            self.assertTrue(ui['name'] in self.ifconfig_ifaces.keys())
            self.assertEqual(ui['type'], self.ifconfig_ifaces[ui['name']]['type'])
            if 'l2_address' in self.ifconfig_ifaces[ui['name']]:
                self.assertEqual(ui['l2_address'], self.ifconfig_ifaces[ui['name']]['l2_address'])

    def test_user_interfaces_number_in_6s_twice_bigger_than_ifconfig_interfaces_number(self):
        time.sleep(6) # 6s
        self.preprocess()
        self.assertEqual(len(self.user_ifaces), len(self.ifconfig_ifaces) * 2)


class TestNetIfaceUserEventsHandling(unittest.TestCase):
    def setUp(self):
        self.user_pid = run_user(LOGFILE_PATH)

    def tearDown(self):
        os.remove(LOGFILE_PATH)

    def test_user_gets_new_iface_event_on_new_veth_link_created(self):
        run_command('ip link add veth0 type veth peer name veth1')
        time.sleep(1) # 1s
        interrupt_program(self.user_pid)
        user_events = parse_user_output(LOGFILE_PATH)[1]
        self.assertTrue({'event_type' : 'NEW', 'iface_name' : 'veth1'} in user_events)
        run_command('ip link del veth0')

    def test_user_gets_gone_iface_event_on_veth_link_removal(self):
        run_command('ip link add veth0 type veth peer name veth1')
        time.sleep(1) # 1s
        run_command('ip link del veth0')
        time.sleep(1) # 1s
        interrupt_program(self.user_pid)
        user_events = parse_user_output(LOGFILE_PATH)[1]
        self.assertTrue({'event_type' : 'GONE', 'iface_name' : 'veth'} in user_events)


if __name__ == '__main__':
    unittest.main()
