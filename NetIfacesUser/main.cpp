#include "InterfaceManager.h"
#include <signal.h>
#include <unistd.h>

#include <queue>
#include <iostream>
#include <mutex>
#include <thread>


std::mutex print_mutex;
bool interrupted = false;


void signal_handler(int signal_number) {
  if (signal_number == SIGINT || signal_number == SIGTERM) {
    interrupted = true;
  }
}


void PrintIfacesList(InterfaceManager* iface_manager) {
  std::vector<Iface> ifaces;
  if (!iface_manager->GetInterfacesList(&ifaces)) {
    std::lock_guard<std::mutex> print_locker(print_mutex);
    std::cerr << "Could not fetch interfaces list." << std::endl;
    return;
  }

  std::lock_guard<std::mutex> print_locker(print_mutex);
  for (auto& iface : ifaces) {
    std::cout << iface << std::endl;
  }
  std::cout << std::endl;
}


void ListenIfaceEvents(InterfaceManager* iface_manager) {
  std::queue<IfaceEvent> iface_events;
  std::mutex mutex;
  std::condition_variable not_empty_cond;
  std::thread changes_getter(&InterfaceManager::GetChanges, iface_manager, &iface_events, &mutex,
                             &not_empty_cond);
  while (!interrupted) {
    std::unique_lock<std::mutex> check_queue_lock(mutex);
    not_empty_cond.wait(check_queue_lock, [&iface_events] () { return !iface_events.empty(); });
    IfaceEvent new_event = iface_events.front();
    iface_events.pop();
    check_queue_lock.unlock();
    std::lock_guard<std::mutex> print_locker(print_mutex);
    std::cout << new_event << std::endl;
    std::cout << std::endl;
  }
  iface_manager->Interrupt();
  changes_getter.join();
}


int main() {
  if (signal(SIGINT, signal_handler) == SIG_ERR || 
      signal(SIGTERM, signal_handler) == SIG_ERR) {
    std::cerr << "Failed to bind signals handler" << std::endl;
    return 1;
  }

  InterfaceManager iface_manager;
  std::thread events_listener(ListenIfaceEvents, &iface_manager);

  while(!interrupted) {
    PrintIfacesList(&iface_manager);
    usleep(5000000); // 5s
  }

  events_listener.join();
  std::cout << "Exiting..." << std::endl;

  return 0;
}
