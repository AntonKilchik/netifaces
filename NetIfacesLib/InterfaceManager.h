#ifndef INTERFACEMANAGER_H
#define INTERFACEMANAGER_H

#include <assert.h>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <string>
#include <vector>
#include "IfaceEvent.h"


class ifinfomsg;


class InterfaceManager {
public:
  InterfaceManager();
  bool GetInterfacesList(std::vector<Iface>* ifaces);
  void GetChanges(std::queue<IfaceEvent>* iface_events, std::mutex* mutex, std::condition_variable* not_empty_cond);
  void Interrupt();

private:
  std::string GetIfaceTypeName(const std::string& iface_name);
  size_t GetIfaceTypeNumber(const std::string& iface_name);
  std::string GetL2Address(const std::string& iface_name);
  std::string ParseL2Address(void* l2_address_ptr);
  void ParseNLAttributes(ifinfomsg* iface_info, size_t nl_message_length, IfaceEvent* iface_event);

  std::recursive_mutex guard_;
  bool interrupted_;
};

#endif // INTERFACEMANAGER_H
