#include "IfaceEvent.h"


bool Iface::operator== (const Iface& another_iface) {
  return name == another_iface.name &&
         type == another_iface.type &&
         l2_address == another_iface.l2_address;
}
