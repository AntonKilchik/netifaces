#include "InterfaceManager.h"

#include <ifaddrs.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <algorithm>
#include <iomanip>
#include <functional>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const size_t INVALID_IFACE_TYPE = static_cast<size_t>(-1);


class CallOnDestroy {
public:
  CallOnDestroy(const std::function<void()>& f)
    : f_(f) {
  }
  ~CallOnDestroy() {
    f_();
  }

private:
  std::function<void()> f_;
};


InterfaceManager::InterfaceManager()
  : interrupted_(false) {
}

bool InterfaceManager::GetInterfacesList(std::vector<Iface>* ifaces) {
  if (ifaces == nullptr) {
    return false;
  }

  if (!ifaces->empty()) {
    ifaces->clear();
  }

  ifaddrs* iface_address = nullptr;
  {
    std::lock_guard<std::recursive_mutex> locker(guard_);
    if (getifaddrs(&iface_address) != -0) {
      return false;
    }
  }

  CallOnDestroy iface_cleaner([iface_address] () { freeifaddrs(iface_address); });

  for (; iface_address != nullptr; iface_address = iface_address->ifa_next) {
    Iface iface;
    iface.name = iface_address->ifa_name;
    iface.l2_address = GetL2Address(iface.name);
    iface.type = GetIfaceTypeName(iface.name);
    if (std::find(ifaces->begin(), ifaces->end(), iface) == ifaces->end()) {
      ifaces->push_back(iface);
    }
  }

  return true;
}


void InterfaceManager::GetChanges(std::queue<IfaceEvent>* iface_events, std::mutex* mutex,
                                  std::condition_variable* not_empty_cond) {
  if (iface_events == nullptr) {
    return;
  }

  const int netlink_socket = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
  if (netlink_socket < 0) {
    std::cerr << "Failed to create netlink socket." << std::endl;
    return;
  }

  sockaddr_nl socket_descr;
  socket_descr.nl_family = AF_NETLINK;
  socket_descr.nl_pad = 0;
  socket_descr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR;
  socket_descr.nl_pid = getpid();
  {
    std::lock_guard<std::recursive_mutex> locker(guard_);
    if (bind(netlink_socket, reinterpret_cast<sockaddr*>(&socket_descr), 
        sizeof(socket_descr)) < 0) {
      std::cerr << "Failed to bind to netlink socket." << std::endl;
      return;
    }
  }
  CallOnDestroy nl_socket_closer([&netlink_socket] () { close(netlink_socket); });

  char payload[8192];
  iovec iov;
  iov.iov_base = payload;
  iov.iov_len = sizeof(payload);

  while (!interrupted_) {
    msghdr msg;
    msg.msg_name = &socket_descr;
    msg.msg_namelen = sizeof(socket_descr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    ssize_t msg_len = recvmsg(netlink_socket, &msg, MSG_DONTWAIT);
    if (msg_len == -1) {
      usleep(1000000); // 1s
      continue;
    }

    for (nlmsghdr* nl_msg_header = reinterpret_cast<nlmsghdr*>(payload); NLMSG_OK(nl_msg_header, msg_len);
                                                        nl_msg_header = NLMSG_NEXT(nl_msg_header, msg_len)) {
      
      size_t nl_msg_len = nl_msg_header->nlmsg_len;
      IfaceEvent iface_event;

      switch (nl_msg_header->nlmsg_type) {
      case RTM_DELLINK:
        iface_event.type = IfaceEvent::Type::GONE;
        break;

      case RTM_NEWLINK:
        iface_event.type = IfaceEvent::Type::NEW;
        break;

      default:
        continue;
      }

      ifinfomsg* iface_info = static_cast<ifinfomsg*>NLMSG_DATA(nl_msg_header);
      ParseNLAttributes(iface_info, nl_msg_len, &iface_event);

      std::lock_guard<std::mutex> queue_lock(*mutex);
      iface_events->push(iface_event);
      not_empty_cond->notify_one();
    }
  }
}


void InterfaceManager::Interrupt() {
  interrupted_ = true;
}


std::string InterfaceManager::GetIfaceTypeName(const std::string& iface_name) {
  std::lock_guard<std::recursive_mutex> locker(guard_);
  const size_t type = GetIfaceTypeNumber(iface_name);
  switch (type) {
  case INVALID_IFACE_TYPE:
    return "";

  case ARPHRD_ETHER:
  case ARPHRD_EETHER:
    return "Ethernet";
  
  case ARPHRD_TUNNEL:
  case ARPHRD_TUNNEL6:
  case ARPHRD_IPDDP:
    return "Tunnel";

  case ARPHRD_LOOPBACK:
    return "Local Loopback";

  default:
  {
    // see linux/if_arp.h for more info about the interface
    std::stringstream ss;
    ss << type;
    return std::string("Type #") + ss.str();
  }
  }
}


size_t InterfaceManager::GetIfaceTypeNumber(const std::string& iface_name) {
  std::lock_guard<std::recursive_mutex> locker(guard_);
  const std::string type_filename = std::string("/sys/class/net/") + iface_name + "/type";
  std::ifstream type_file(type_filename);
  if (!type_file.is_open()) {
    return INVALID_IFACE_TYPE;
  }
    
  std::string type_str;
  std::getline(type_file, type_str);
  size_t type = INVALID_IFACE_TYPE;
  std::stringstream(type_str) >> type;
  return type;
}


std::string InterfaceManager::GetL2Address(const std::string& iface_name) {
  std::lock_guard<std::recursive_mutex> locker(guard_);
  int ioctl_socket = socket(PF_INET, SOCK_DGRAM, 0);
  if (ioctl_socket < 0) {
    std::cerr << "Failed to create ioctl socket" << std::endl;
    return "";
  }
  CallOnDestroy socket_closer([&ioctl_socket] () { close(ioctl_socket); });

  ifreq buffer;
  memset(&buffer, 0, sizeof(buffer));
  strcpy(buffer.ifr_name, iface_name.c_str());
  ioctl(ioctl_socket, SIOCGIFHWADDR, &buffer);

  return ParseL2Address(buffer.ifr_hwaddr.sa_data);
}


std::string InterfaceManager::ParseL2Address(void* l2_address_ptr) {
  std::lock_guard<std::recursive_mutex> locker(guard_);
  const size_t kBufferSize = 64;
  char buffer[kBufferSize];
  unsigned char* ptr = reinterpret_cast<unsigned char*>(l2_address_ptr);
  snprintf(buffer, kBufferSize, " %02x:%02x:%02x:%02x:%02x:%02x", ptr[0], ptr[1], ptr[2], ptr[3], ptr[4], ptr[5]);
  return { buffer };
}


void InterfaceManager::ParseNLAttributes(ifinfomsg* iface_info, size_t nl_message_length, IfaceEvent* iface_event) {
  for (rtattr* cur_attr = IFLA_RTA(iface_info); RTA_OK(cur_attr, nl_message_length); 
                                              cur_attr = RTA_NEXT(cur_attr, nl_message_length)) {
    if (cur_attr->rta_type > IFLA_MAX) {
      break;
    }

    if (cur_attr->rta_type != IFLA_IFNAME) {
      continue;
    }

    iface_event->iface.name = reinterpret_cast<char*>(RTA_DATA(cur_attr));
    if (iface_event->iface.name == "") {
      continue;
    }

    if (iface_event->type == IfaceEvent::Type::NEW) {
      iface_event->iface.type = GetIfaceTypeName(iface_event->iface.name);
      iface_event->iface.l2_address = GetL2Address(iface_event->iface.name);
    }
  }  
}
