#ifndef IFACEEVENT_H
#define IFACEEVENT_H

#include <assert.h>
#include <string>


struct Iface {
  bool operator== (const Iface& another_iface);

  std::string name;
  std::string type; // ethernet / loopback / tun
  std::string l2_address;
};


template <typename T>
T& operator<< (T& os, const Iface& iface) {
  os << "IFACE " << iface.name << " " << iface.l2_address << " " << iface.type;
  return os;
}


struct IfaceEvent {
  enum class Type {
    GONE,
    NEW
  };
  Iface iface;
  Type type;
};


template <typename T>
T& operator<< (T& os, const IfaceEvent& event) {
  switch(event.type) {
  case IfaceEvent::Type::NEW:
    os << "NEW " << event.iface.name << " " << event.iface.l2_address << " " << event.iface.type;
    break;
  case IfaceEvent::Type::GONE:
    os << "GONE " << event.iface.name;
    break;
  default:
    assert(!"unexpected event");
    break;
  }
  return os;
}

#endif // IFACEEVENT_H
